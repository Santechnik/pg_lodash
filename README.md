
# LodashPG

` LodashPG is a freely distributed collection of useful utility functions. `

Copyright © 2021 Santechnik Intarray. https://gitlab.com/Santechnik/pg_lodash 

## Disclaimer
The Author makes no representations or warranties, express or implied, with respect to the content, accuracy, truthfulness or reliability of any functionality or information contained in any document which is part of the LodashPG code library, whether in full or in part, including any warranty of title, non-infringement of copyright or any other rights of others, merchantability, or fitness or suitability for any purpose. Without limiting the generality of the foregoing, by using or attempting to use this code library, the user expressly acknowledges that there are no warranties or representations made by the Author regarding the content of this code library in terms of its accuracy or its completeness. The Author does not control or warrant the availability or content of any external website or other resource to which this website links. In no event shall the Author be liable for any damages whatsoever resulting from, arising out of or in connection with the use of any part of LodashPG code library. Furthermore, the Author shall not be liable for any damages or losses suffered owing to any interruption in service or use of this code library.


## Functions

### calculate_annual_pro_rate

> Calculates the annual pro-rate ratio, e.g. full year is 1, 6 months is 0.5, etc.

##### Parameters: 

1. p_current_expiry timestamp with time zone

1. p_new_expiry timestamp with time zone

##### Returns: 


1. numeric

---

### calculate_billing_cycle

> Calculates billing cycle based on the billing day and the reference date. For example, if today is 12th of December and the billing day is 4th of the month, the billing cycle will be DEC 4th to JAN 4th. If the billing day is the 18th, the billing cycle will be NOV 18th to DEC 18th.

##### Parameters: 

1. p_cycle_day integer

1. p_ref_date date DEFAULT now()

##### Returns (table): 


1. cycle_start_day date

1. cycle_end_day date

---

### eval

> Evaluates an expression supplies as text. The same apply as when a SELECT query is composed (because this is basically what this is). This is a very dangerous function. EXAMPLE: select _pg.eval('5+6 < 9') -- Returns false

##### Parameters: 

1. expression text

##### Returns: 


1. text

---

### execute_shell_command

> Executes a Linux shell command. The user must be a SUPERUSER and database must be running on Linux. This is an extremely dangerous command. Use wisely.

##### Parameters: 

1. p_command text

##### Returns: 


1. text

---

### generate_column_list_for_table

> Generates a SQL-ready list of columns for a table.

##### Parameters: 

1. p_schema_name text

1. p_table_name text

1. p_is_formatted boolean DEFAULT false

1. p_exlude_columns text[] DEFAULT NULL::text[]

##### Returns: 


1. text

---

### generate_create_table_statement

> Generates a create table statement. This works good for simpler tables. Results generated for complicated tables, such as partitioned, etc., may need to be adjusted

##### Parameters: 

1. p_table_name text

##### Returns: 


1. text

---

### generate_digit_string

> Generates a random string of specified length filled with random digits. This is useful when generating Multi-Factor Authentication tokens.

##### Parameters: 

1. p_number_of_positions integer

##### Returns: 


1. text

---

### generate_md_docs

> Generates Markdown documentation for functions in the specified schema, as well as a list of parameters.

##### Parameters: 

1. p_schema_name text

##### Returns (table): 


1. schema_name text

1. function_name text

1. full_function_name text

1. description text

1. params text[]

1. param_names text

1. param_types text

1. md_doc text

---

### generate_random_mac_address

> Generate a random MAC address, optionally starting from a specific character.

##### Parameters: 

1. p_first_char character DEFAULT NULL::bpchar

##### Returns: 


1. text

---

### generate_random_string

> Generates a random strong of given length. No guarantees are given as to true randomness.

##### Parameters: 

1. p_length integer

##### Returns: 


1. text

---

### generate_serial_num

> Generates a random serial number of the REII-16919 format.

##### Parameters: 

1. p_prefix text DEFAULT 'R'::text

##### Returns: 


1. text

---

### get_all_view_dependents

> List all objects which are dependent on the view

##### Parameters: 

1. p_schema text

1. p_view_name text

##### Returns (table): 


1. dependent_schema text

1. dependent_view text

---

### get_partitioned_table_children

> Lists all partitions for a table

##### Parameters: 

1. p_schema_name text

1. p_table_name text

##### Returns (table): 


1. schema_name text

1. parent_table text

1. child_table text

---

### get_table_create_order

> Returns the list of tables in the order they need to be created due to dependencies

##### Parameters: 

1. p_schema_name text

##### Returns (table): 


1. schema_name text

1. parent_table text

1. create_order bigint

---

### get_type_values

> Lists all values for an enum type

##### Parameters: 

1. p_type_name name

##### Returns: 


1. text[]

---

### get_uuid

> Generates a UUID. Shorter version of uuid_generate_v4().

##### Parameters: 

_none_

##### Returns: 


1. uuid

---

### get_version

> 

##### Parameters: 

_none_

##### Returns: 


1. text

---

### hex_to_int

> Returns the integer value of the parameter supplied in HEX.

##### Parameters: 

1. hexval character varying

##### Returns: 


1. bigint

---

### is_numeric

> Checks if the supplied parameter is numeric.

##### Parameters: 

1. p_str text

##### Returns: 


1. boolean

---

### is_table_a_child_partition

> Checks whether the specified table is a partition of another table

##### Parameters: 

1. p_schema_name text

1. p_table_name text

##### Returns: 


1. boolean

---

### is_valid_sim_card

> Checks if a valid SIM card number is supplied using the Luhn algorithm. As per https://www.red-gate.com/simple-talk/blogs/the-luhn-algorithm-in-sql/ 

##### Parameters: 

1. p_iccid bytea

##### Returns: 


1. boolean

---

### is_valid_sim_card

> Checks if a valid SIM card number is supplied using the Luhn algorithm. As per https://www.red-gate.com/simple-talk/blogs/the-luhn-algorithm-in-sql/ 

##### Parameters: 

1. p_iccid text

##### Returns: 


1. boolean

---

### last_day_of_month

> Retrieves the last day of the given month.

##### Parameters: 

1. p_date date

##### Returns: 


1. date

---

### leading_zero

> Adds the specified number of leading zeros.

##### Parameters: 

1. p_num integer

1. p_positions integer

##### Returns: 


1. text

---

### strpos_ex

> An extension of strpos - adds an optional parameter to specify where to start looking in order to find p_find in p_str.

##### Parameters: 

1. p_str text

1. p_find text

1. p_start integer DEFAULT 0

##### Returns: 


1. integer

---

### text_to_bigint

> Converts text to bigint. If the conversion fails, returns the second argument

##### Parameters: 

1. text

1. bigint

##### Returns: 


1. bigint

---

### text_to_date

> Converts text to timestamp with timezone. If the conversion fails, returns the second argument

##### Parameters: 

1. text

1. timestamp with time zone

##### Returns: 


1. timestamp with time zone

---

### text_to_float

> Converts text to double. If the conversion fails, returns the second argument

##### Parameters: 

1. text

1. double precision

##### Returns: 


1. double precision

---

### text_to_int

> Converts text to integer. If the conversion fails, returns the second argument

##### Parameters: 

1. text

1. integer

##### Returns: 


1. integer

---

### time_diff

> Calculates the difference in ms between two timestamps.

##### Parameters: 

1. p_time1 timestamp with time zone

1. p_time2 timestamp with time zone

##### Returns: 


1. bigint

---

