drop function if exists _pg.generate_schema_erd_diagram;

CREATE OR REPLACE FUNCTION _pg.generate_schema_erd_diagram(p_schema_name text, p_include_md_delimiter boolean default false)
 RETURNS text
 LANGUAGE sql
AS $function$
	with all_tables as
	(
		select	table_schema,
				table_name
			from information_schema.tables
			where table_schema = p_schema_name
				and not _pg.is_table_a_child_partition(table_schema, table_name)
	),
	links as
	(
		select 	t.table_name || ' }o--o| ' || dep.linked_fk_table || ' : ' || fk_name as fk_link
			from all_tables t
			join _pg.get_table_foreign_keys(table_schema, table_name) dep on true
	),
	col_defs as
	(
		select	t.table_name,
				c.column_name || ' ' || (split_part(c.data_type, ' ', 1)) as col_definition
			from all_tables t
			join information_schema."columns" c on c.table_name = t.table_name and c.table_schema = t.table_schema
			order by t.table_name, c.ordinal_position
	),
	table_definitions as
	(
		select	table_name || ' {' || chr(13) || (string_agg(col_definition, chr(13))) || chr(13) || '}' as table_def
			from col_defs
			group by table_name
	)
	select	(
				select (case when p_include_md_delimiter then '```mermaid' else '' end)
						||chr(13)||'erDiagram'||chr(13)
						|| string_agg(l.fk_link, chr(13))
						|| chr(13) as mermaid_code
					from links l
			)
			||
			(
				select string_agg(td.table_def, chr(13)) 
					   || chr(13) 
					   || (case when p_include_md_delimiter then '```' else '' end) as mermaid_code
					from table_definitions td
			) as mermaid_code;
$function$
;

COMMENT ON FUNCTION "_pg".generate_schema_erd_diagram IS 'Generates the mermaid ERD diagram for the specified schema. The links are specified as optional one-to-many, as this seems to be a good compromise. The actual relationship is not being determined in this version.';
