drop function if exists _pg.generate_random_mac_address;

CREATE OR REPLACE FUNCTION _pg.generate_random_mac_address(p_first_char character DEFAULT NULL::bpchar, p_delimiter character default '-'::bpchar)
 RETURNS text
 LANGUAGE sql
AS $function$
with list_of_chars as
(
	select	chr(generate_series) as macchar
		from generate_series(48,57)
	union
	select	chr(generate_series) as macchar
		from generate_series(65,70)	
),
rand_list as
(
	select 	lc.macchar,
			row_number () over (order by random()) as rn
		from list_of_chars lc
		join generate_series(0, 1000) gs on 1=1
		limit 12
)
select coalesce(p_first_char, (array_agg(macchar))[1])||(array_agg(macchar))[2]||p_delimiter
		||(array_agg(macchar))[3]||(array_agg(macchar))[4]||p_delimiter
		||(array_agg(macchar))[5]||(array_agg(macchar))[6]||p_delimiter
		||(array_agg(macchar))[7]||(array_agg(macchar))[8]||p_delimiter
		||(array_agg(macchar))[9]||(array_agg(macchar))[10]||p_delimiter
		||(array_agg(macchar))[11]||(array_agg(macchar))[12] as macaddr
	from rand_list
$function$
;

COMMENT ON FUNCTION "_pg".generate_random_mac_address IS 'Generate a random MAC address, optionally starting from a specific character.';