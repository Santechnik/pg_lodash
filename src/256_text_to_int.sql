CREATE OR REPLACE FUNCTION _pg.text_to_int(text, integer)
 RETURNS integer
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
    return cast($1 as integer);
exception
    when invalid_text_representation then
        return $2;
end;
$function$
;

comment on function _pg.text_to_int is 'Converts text to integer. If the conversion fails, returns the second argument';
