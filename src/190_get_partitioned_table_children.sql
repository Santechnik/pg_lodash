
CREATE OR REPLACE FUNCTION _pg.get_partitioned_table_children(p_schema_name text, p_table_name text)
 RETURNS TABLE(schema_name text, parent_table text, child_table text)
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
	SELECT
	    nmsp_parent.nspname::text AS schema_name,
	    parent.relname::text      AS parent,
	    child.relname::text       AS child
	FROM pg_inherits
	    JOIN pg_class parent            ON pg_inherits.inhparent = parent.oid
	    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid
	    JOIN pg_namespace nmsp_parent   ON nmsp_parent.oid  = parent.relnamespace
	    JOIN pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
	WHERE parent.relname=p_table_name
		and nmsp_parent.nspname::text = p_schema_name;
$function$
;

comment on function _pg.get_partitioned_table_children is 'Lists all partitions for a table';