CREATE OR REPLACE FUNCTION _pg.hex_to_int(hexval character varying)
 RETURNS bigint
 LANGUAGE plpgsql
 IMMUTABLE STRICT
AS $function$
DECLARE
    result  bigint;
BEGIN
    EXECUTE 'SELECT x' || quote_literal(hexval) || '::bigint' INTO result;
    RETURN result;
END;
$function$
;

comment on function _pg.hex_to_int is 'Returns the integer value of the parameter supplied in HEX.';