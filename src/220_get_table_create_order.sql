
CREATE OR REPLACE FUNCTION _pg.get_table_create_order(p_schema_name text)
 RETURNS TABLE(schema_name text, parent_table text, create_order bigint)
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
with recursive fk_tree as 
(
  -- All tables not referencing anything else
  select t.oid as reloid, 
         t.relname as table_name, 
         s.nspname as schema_name,
         null::text COLLATE "C" as referenced_table_name,
         null::text COLLATE "C" as referenced_schema_name,
         1 as level
  from pg_class t
    join pg_namespace s on s.oid = t.relnamespace
  where relkind = 'r'
    and not exists (select *
                    from pg_constraint
                    where contype = 'f'
                      and conrelid = t.oid)
    and s.nspname::text = p_schema_name -- limit to one schema 
  union all 
  select ref.oid, 
         ref.relname, 
         rs.nspname,
         p.table_name COLLATE "C" ,
         p.schema_name COLLATE "C" ,
         p.level + 1
  from pg_class ref
    join pg_namespace rs on rs.oid = ref.relnamespace
    join pg_constraint c on c.contype = 'f' and c.conrelid = ref.oid
    join fk_tree p on p.reloid = c.confrelid
), 
all_tables as 
(
  -- this picks the highest level for each table
  select schema_name, table_name,
         level, 
         row_number() over (partition by schema_name, table_name order by level desc) as last_table_row
  from fk_tree
),
table_list as
(
	select 	schema_name, 
			table_name, 
			(select * from _pg.is_table_a_child_partition(schema_name, table_name)) as is_a_partition,
			(select count(*) from _pg.get_partitioned_table_children(schema_name, table_name)) as child_count,
			level as lvl
		from all_tables at
		where last_table_row = 1
		order by level
)
select	schema_name::text,
		table_name::text,
		rank() over (order by lvl, table_name) as create_order
	from table_list
	where not is_a_partition;
	$function$
;

comment on function _pg.get_table_create_order is 'Returns the list of tables in the order they need to be created due to dependencies';