create or replace function _pg.num_to_icon_text(p_num integer)
returns text 
language plpgsql
as 
$$
declare 
	_chars 		text[];
	_text_str	text;
	_chrNum		int;
	_counter	int;
	_res		text;
begin 
	_chars := array[':zero:', ':one:', ':two:', ':three:', ':four:', ':five:', ':six:', ':seven:', ':eight:', ':nine:'];
	_res := '';
	_text_str := p_num::text;

	for _counter in 1 .. length(_text_str)
	loop
		_res := _res || format('%s', _chars[substring(_text_str, _counter, 1)::int + 1]);
	end loop;

	return _res::text;
end;
$$;

comment on function  _pg.num_to_icon_text is 'Converts a number into the "icon text", e.g. 123 into :one::two::three:';
