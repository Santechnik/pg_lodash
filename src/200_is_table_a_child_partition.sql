
CREATE OR REPLACE FUNCTION _pg.is_table_a_child_partition(p_schema_name text, p_table_name text)
 RETURNS boolean
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
select exists(
	select *
		from pg_inherits
		where inhrelid = (p_schema_name || '.' || p_table_name)::regclass);
$function$
;

comment on function _pg.is_table_a_child_partition is 'Checks whether the specified table is a partition of another table';