create or replace function _pg.generate_ipv4_address()
returns text
language sql
immutable
parallel safe
as
$$
	select (select generate_series from generate_series(1, 255) order by 1 * random() limit 1) || '.' || (select generate_series from generate_series(1, 255) order by 1 * random() limit 1) || '.' || (select generate_series from generate_series(1, 255) order by 1 * random() limit 1) || '.' || (select generate_series from generate_series(1, 255) order by 1 * random() limit 1)
$$;

comment on function _pg.generate_ipv4_address is 'Generates a random IPv4 address';
