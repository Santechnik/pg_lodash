CREATE OR REPLACE FUNCTION _pg.calculate_billing_cycle(p_cycle_day integer, p_ref_date date DEFAULT now())
 RETURNS TABLE(cycle_start_day date, cycle_end_day date)
language plpgsql
immutable
parallel safe
as
$$
declare
	_today_day				integer;

	_adj_cycle_start_day	integer;
	_prelim_cycle_start		date;
	_start_date				date;
	_end_date				date;
begin
	_today_day := date_part('day', p_ref_date)::int;

	-- Create a random date in the appropriate month
	if (p_cycle_day > _today_day) then
		-- Check if this cycle going to start in the previous month
		_prelim_cycle_start := p_ref_date - interval '1 month';
	else
		_prelim_cycle_start := p_ref_date;
	end if;
	
	-- If the cycle start date is past the last day of the month - make it the last day of that month
	if (p_cycle_day > date_part('day', _pg.last_day_of_month(_prelim_cycle_start))) then
		_adj_cycle_start_day := date_part('day', _pg.last_day_of_month(_prelim_cycle_start));
	else
		_adj_cycle_start_day := p_cycle_day;
	end if;
	-- Create the start date
	_start_date := make_date(date_part('year', _prelim_cycle_start)::int, date_part('month', _prelim_cycle_start)::int, _adj_cycle_start_day);

	-- Normally, the end date is a month after the start date. Also the day after that is the start of another cycle.
	_end_date := _start_date + interval '1 month' - interval '1 day';	
	
	-- However, if the start date is the last day of that months, we'll assume that we need the end date to also be the last day of the month
	if (_pg.last_day_of_month(_start_date) = _start_date) then
		_end_date := _pg.last_day_of_month(_end_date);
	end if;

	return query
		select _start_date, _end_date::date;
end;
$$;


/*
select cycle_start_day = '2023-05-01' and cycle_end_day = '2023-05-31' from _pg.calculate_billing_cycle(1, '2023-MAY-02'); -- 2023-05-01	2023-05-31
select cycle_start_day = '2023-05-01' and cycle_end_day = '2023-05-31' from _pg.calculate_billing_cycle(1, '2023-MAY-22'); -- 2023-05-01	2023-05-31
select cycle_start_day = '2023-05-01' and cycle_end_day = '2023-05-31' from _pg.calculate_billing_cycle(1, '2023-MAY-31'); -- 2023-05-01	2023-05-31
select cycle_start_day = '2023-04-01' and cycle_end_day = '2023-04-30' from _pg.calculate_billing_cycle(1, '2023-APR-28'); -- 2023-04-01	2023-04-30
select cycle_start_day = '2023-05-01' and cycle_end_day = '2023-05-31' from _pg.calculate_billing_cycle(1, '2023-MAY-01'); -- 2023-05-01	2023-05-31
select cycle_start_day = '2023-04-09' and cycle_end_day = '2023-05-08' from _pg.calculate_billing_cycle(9, '2023-MAY-01'); -- 2023-04-09	2023-05-08
select cycle_start_day = '2023-03-31' and cycle_end_day = '2023-04-30' from _pg.calculate_billing_cycle(31, '2023-APR-28'); -- 2023-03-31	2023-04-30
select cycle_start_day = '2023-04-28' and cycle_end_day = '2023-05-27' from _pg.calculate_billing_cycle(28, '2023-APR-28'); -- 2023-04-28	2023-05-27
select cycle_start_day = '2023-03-04' and cycle_end_day = '2023-04-03' from _pg.calculate_billing_cycle(04, '2023-APR-03'); -- 2023-03-04	2023-04-02
select cycle_start_day = '2023-04-14' and cycle_end_day = '2023-05-13' from _pg.calculate_billing_cycle(14, '2023-MAY-02'); -- 2023-04-14	2023-05-13
select cycle_start_day = '2023-04-30' and cycle_end_day = '2023-05-31' from _pg.calculate_billing_cycle(31, '2023-MAY-02'); -- 2023-04-30	2023-05-31
select cycle_start_day = '2023-02-01' and cycle_end_day = '2023-02-28' from _pg.calculate_billing_cycle(01, '2023-FEB-14'); -- 2023-02-01	2023-02-28
select cycle_start_day = '2023-01-31' and cycle_end_day = '2023-02-28' from _pg.calculate_billing_cycle(31, '2023-FEB-14'); -- 2023-01-31	2023-02-28
*/

comment on function _pg.calculate_billing_cycle is 'Calculates billing cycle based on the billing day and the reference date. For example, if today is 12th of December and the billing day is 4th of the month, the billing cycle will be DEC 4th to JAN 4th. If the billing day is the 18th, the billing cycle will be NOV 18th to DEC 18th. Note that the end day is assumed to be the last day of the month if the start date falls on the last day of the month.';

