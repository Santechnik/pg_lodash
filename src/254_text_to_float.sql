CREATE OR REPLACE FUNCTION _pg.text_to_float(text, double precision)
 RETURNS double precision
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
    return cast($1 as float);
exception
    when invalid_text_representation then
        return $2;
end;
$function$
;

comment on function _pg.text_to_float is 'Converts text to double. If the conversion fails, returns the second argument';
