
CREATE OR REPLACE FUNCTION _pg.generate_column_list_for_table(p_schema_name text, p_table_name text, p_is_formatted boolean DEFAULT false, p_exlude_columns text[] DEFAULT NULL::text[])
 RETURNS text
 LANGUAGE sql
AS $function$
	select 	array_to_string(array_agg(cols.column_name), (case when p_is_formatted then ','||chr(13) else ', ' end) ) as col_names
	    from information_schema.columns cols
	    where cols.table_schema = p_schema_name
	  		and cols.table_name = p_table_name
	  		and cols.is_generated <> 'ALWAYS'
	  		and cols.column_name not in (select unnest(p_exlude_columns));
$function$
;

comment on function _pg.generate_column_list_for_table is 'Generates a SQL-ready list of columns for a table.';
