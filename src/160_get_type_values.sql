create or replace function _pg.get_type_values(p_type_name name)
returns text[]
language sql
as 
$$
	with type_vals as
	(
		select e.enumlabel::text
		from pg_type t 
		   join pg_enum e on t.oid = e.enumtypid  
		   join pg_catalog.pg_namespace n ON n.oid = t.typnamespace
		 	where t.typname = p_type_name
		 order by e.enumsortorder 
	)
	select array_agg(tv.enumlabel) as type_values
		from type_vals tv;
$$;	

comment on function _pg.get_type_values is 'Lists all values for an enum type';