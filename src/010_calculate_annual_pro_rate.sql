/**
Calculates pro rate based on annual renewal

Relies on time_diff function
*/

CREATE OR REPLACE FUNCTION _pg.calculate_annual_pro_rate(p_current_expiry timestamp with time zone, p_new_expiry timestamp with time zone)
 RETURNS numeric
 LANGUAGE sql
AS $function$
	select	( 	(floor(_pg.time_diff(p_new_expiry, now()) / 86400.0))
					- 
				(floor(_pg.time_diff(p_current_expiry, now()) / 86400.0))
		    ) / 365.0;
$function$
;

comment on function _pg.calculate_annual_pro_rate is 'Calculates the annual pro-rate ratio, e.g. full year is 1, 6 months is 0.5, etc.';
