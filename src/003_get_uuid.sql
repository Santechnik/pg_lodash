create or replace function _pg.get_uuid()
returns uuid
language plpgsql
as
$$
begin
    return gen_random_uuid();
end;
$$ ;

comment on function _pg.get_uuid is 'Generates a UUID';