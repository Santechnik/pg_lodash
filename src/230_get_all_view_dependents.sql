
CREATE OR REPLACE FUNCTION _pg.get_all_view_dependents(p_schema text, p_view_name text)
 RETURNS TABLE(dependent_schema text, dependent_view text)
 LANGUAGE sql
 SECURITY DEFINER
AS $function$
SELECT  dependent_ns.nspname::text as dependent_schema,
        dependent_view.relname::text as dependent_view
    FROM pg_depend 
    JOIN pg_rewrite ON pg_depend.objid = pg_rewrite.oid 
    JOIN pg_class as dependent_view ON pg_rewrite.ev_class = dependent_view.oid 
    JOIN pg_class as source_table ON pg_depend.refobjid = source_table.oid 
    JOIN pg_namespace dependent_ns ON dependent_ns.oid = dependent_view.relnamespace
    JOIN pg_namespace source_ns ON source_ns.oid = source_table.relnamespace
    WHERE 
    source_ns.nspname = p_schema
    AND source_table.relname = p_view_name
    and dependent_view.relname <> p_view_name
    ORDER BY 1,2;
$function$
;

comment on function _pg.get_all_view_dependents is 'List all objects which are dependent on the view';