create or replace function _pg.generate_md_docs(p_schema_name text)
returns table (schema_name text, function_name text, full_function_name text, description text, params text[], param_names text, param_types text, md_doc text)
language sql 
as 
$$
	with src as
	(
		SELECT 	(p.pronamespace::regnamespace)::text as schema_name,
				p.proname::text as function_name,
				(p.pronamespace::regnamespace)::text || '.' || p.proname::text as full_function_name,
				od.description as description,
				string_to_array(pg_get_function_identity_arguments(p.oid), ', ') as params,
		        replace(pg_get_function_arguments(p.oid), ', ', chr(10) || chr(13) || '1. ') AS doc_params,
		        src.source_code,
		        strpos(src.source_code, 'RETURNS ') + 8 as ret_pos,
		        strpos(src.source_code, 'LANGUAGE ') - 2 as lan_pos
		    FROM pg_proc p
		    join 
		    (
			    	WITH namespaces AS 
			    	(
			        	SELECT 	nsp.oid,
			            		nsp.nspname
			           		FROM pg_namespace nsp
			           		where nsp.nspname = p_schema_name
			        ), 
			        list_functions AS 
			        (
			        	SELECT 	pgp.oid,
			            		nsp.nspname AS schema_name,
			            		pgp.proname AS object_name,
			            		pg_get_functiondef(pgp.oid) AS source_code,
			            		''::text AS comm
			        		FROM pg_proc pgp
			             	JOIN namespaces nsp ON nsp.oid = pgp.pronamespace
			        )
					SELECT	lf.oid,
							lf.schema_name,
							lf.object_name,
							lf.source_code,
							lf.comm
						FROM list_functions lf
		    ) src on src.schema_name = (p.pronamespace::regnamespace)::text and src.object_name = p.proname
			left join pg_description od on od.objoid = p.oid
		 	order by src.schema_name
	),
	proc1 as 
	(
		select 	schema_name,
				function_name,
				full_function_name,
				description,
				params,
				case when doc_params is not null and length(doc_params) > 2 then '1. '||doc_params else '_none_' end as doc_params,
				source_code,
				(strpos(source_code, 'TABLE(') > 0) as is_table_return,
				replace(substring(source_code, ret_pos, lan_pos - ret_pos), 'TABLE(', '') as rets
			from src
	),
	proc2 as
	(
		select	schema_name,
				function_name,
				full_function_name,
				description,
				params,
				doc_params,
				is_table_return,
				case when is_table_return then substring(rets, 0, length(rets)) else rets end as rets
			from 
				proc1
	),
	proc3 as 
	(
		select 	schema_name,
				function_name,
				full_function_name,
				description,
				params,
				doc_params,
				is_table_return,
				array_to_string(string_to_array(rets, ', '), chr(10) || chr(13) || '1. ') as rets
			from proc2
	),
	proc_params1 as
	(
		select	schema_name,
			function_name,
			full_function_name,
			description,
			params,
			p.i as split_params,
			'### ' || format('%s.%s', schema_name, function_name) || '' || chr(10) || chr (13) || '> ' || coalesce(description, '') || '' || chr(10) || chr (13) ||'##### Parameters: ' || chr(10) || chr (13) || coalesce(doc_params, 'none') || ''	|| chr(10) || chr (13) || '##### Returns' || (case when is_table_return then ' (table): ' else ': ' end ) || chr(10) || chr (13) || chr(10) || chr (13) || case when rets is not null then '1. ' || rets else rets end || '' || chr(10) || chr (13) || '---' || chr(10) || chr (13) as doc
		from proc3
		left join lateral unnest(params) as p(i) on true
	),
	proc_params2 as
	(
		select	schema_name,
				function_name,
				full_function_name,
				description,
				params,
				split_part(split_params, ' ', 1) as param_name,
		     	split_part(split_params, ' ', 2) as param_type,
		     	doc
			from proc_params1
	),
	proc_params3 as
	(
		select	schema_name,
				function_name,
				full_function_name,
				description,
				params,
				array_agg(param_name) as param_names,
				array_agg(param_type) as param_types,
				doc
			from proc_params2
			group by schema_name, function_name, full_function_name, params, doc, description
	)
	select 	schema_name,
			function_name,
			full_function_name,
			description,
			params,
			array_to_string(param_names, ', ') as param_names,
			array_to_string(param_types, ', ') as param_types,
			doc
		from proc_params3;
$$;

comment on function _pg.generate_md_docs is 'Generates Markdown documentation for functions in the specified schema, as well as a list of parameters.';
