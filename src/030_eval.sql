/**
    Evaluates an expression supplies as text. The same apply as when a SELECT query is composed (because this is basically what this is).

    Because of the dangerous nature of this function, it is normally commented out, uncomment if you're willing to take the risk.

    Examples:
        select eval('5+6') -- Returns 11
        select _pg.eval('5+6 < 9') -- Returns false

*/


CREATE OR REPLACE FUNCTION _pg.eval(expression text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare
  result text;
begin
	result := null;
	
	if (expression is not null) then
  		execute 'select ' || expression into result;
  	end if;
  	
  return result;

exception when others then
	return sqlerrm;
end;
$function$
;

comment on function _pg.eval is 'Evaluates an expression supplies as text. The same apply as when a SELECT query is composed (because this is basically what this is). This is a very dangerous function. EXAMPLE: select _pg.eval(''5+6 < 9'') -- Returns false';
