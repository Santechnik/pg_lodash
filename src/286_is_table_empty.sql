create or replace function _pg.is_table_empty(p_schema_name text, p_table_name text, p_threshold int default 100)
returns bool
language plpgsql
security definer
as 
$$
declare
	_res	bool;
begin
	_res := null;

	execute format('SELECT count(*) = 0 FROM (SELECT 1 FROM %I.%I LIMIT %s) t', p_schema_name, p_table_name, p_threshold) into _res;
	
	return _res;
end;
$$;

comment on function _pg.is_table_empty is 'Checks whether specified table contains rows';
