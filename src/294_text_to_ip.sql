CREATE OR REPLACE FUNCTION _pg.text_to_ip(text, inet)
 RETURNS inet
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
    return cast($1 as inet);
exception
    when invalid_text_representation then
        return $2;
end;
$function$
;

COMMENT ON FUNCTION "_pg".text_to_ip(text, inet) IS 'Converts text to IP address. If the conversion fails, returns the second argument';

