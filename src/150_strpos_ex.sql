/**
An extension of strpos - adds an optional start parameter to specify where to start looking for the p_find in p_str
*/

create or replace function _pg.strpos_ex(p_str text, p_find text, p_start int default 0)
returns int
language sql
as
$$
	with st as 
	(
		select	substring(p_str, p_start, length(p_str)) as ct
	)
	select	case when strpos(ct, p_find) > 0 then strpos(ct, p_find) + p_start else 0 end
		from st;
$$;


comment on function _pg.strpos_ex is 'An extension of strpos - adds an optional parameter to specify where to start looking in order to find p_find in p_str.';
