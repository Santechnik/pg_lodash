CREATE OR REPLACE FUNCTION _pg.text_to_numeric(text, numeric)
 RETURNS double precision
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
    return cast($1 as numeric);
exception
    when invalid_text_representation then
        return $2;
end;
$function$
;
COMMENT ON FUNCTION "_pg".text_to_numeric(text, numeric) IS 'Converts text to numeric. If the conversion fails, returns the second argument';
