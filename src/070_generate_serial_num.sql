create or replace function _pg.generate_serial_num(p_prefix text default 'R')
returns text
language sql	
as
$$
	select p_prefix||chr((random() * 24 + 65)::int) || chr((random() * 24 + 65)::int) || chr((random() * 24 + 65)::int) ||'-'||(random() * 100000 + 1)::int
$$;

comment on function _pg.generate_serial_num is 'Generates a random serial number of the REII-16919 format.';