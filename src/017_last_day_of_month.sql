CREATE OR REPLACE FUNCTION _pg.last_day_of_month(p_date date)
 RETURNS date
 LANGUAGE sql
AS $function$
    SELECT (date_trunc('month', p_date) + interval '1 month' - interval '1 day')::date
AS end_of_month;
$function$
;

comment on function _pg.last_day_of_month is 'Retrieves the last day of the given month.';