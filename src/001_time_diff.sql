create or replace function _pg.time_diff(p_time1 timestamptz, p_time2 timestamptz)
returns bigint
language sql
as 
$$
	select ceiling(EXTRACT(epoch FROM (p_time1 - p_time2)))::bigint;
$$;

comment on function _pg.time_diff is 'Calculates the difference in ms between two timestamps.';
