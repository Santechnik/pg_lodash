create or replace function _pg.generate_digit_string(p_number_of_positions int)
returns text
language sql
as
$$
	select "_pg".leading_zero( (random() * power(10, p_number_of_positions) )::int, p_number_of_positions);
$$;

comment on function _pg.generate_digit_string is 'Generates a random string of specified length filled with random digits. This is useful when generating Multi-Factor Authentication tokens.';
