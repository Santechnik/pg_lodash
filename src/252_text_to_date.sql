CREATE OR REPLACE FUNCTION _pg.text_to_date(text, timestamp with time zone)
 RETURNS timestamp with time zone
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
    return cast($1 as timestamptz);
exception
    when others then
        return $2;
end;
$function$
;

comment on function _pg.text_to_date is 'Converts text to timestamp with timezone. If the conversion fails, returns the second argument';
