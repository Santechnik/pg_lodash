/** Be careful with this due to CASCADE**/
-- drop schema if exists _pg cascade;

create schema if not exists _pg;
comment on schema _pg is 'Lodash PG is a freely distributed collection of useful utility functions. Copyright (c) 2021 Santechnik Intarray. https://gitlab.com/Santechnik/pg_lodash';

