create or replace function _pg.get_table_foreign_keys(p_schema_name text, p_table_name text)
returns table (schema_name text, table_name text, fk_name text, linked_fk_table text)
language sql
as
$$
SELECT	nsp.nspname::text as schema_name,
		rel.relname::text as table_name,
		con.conname::text as fk_name,
		fkt.relname::text as linked_fk_table
	from pg_catalog.pg_constraint con
	join pg_catalog.pg_class rel ON rel.oid = con.conrelid
    join pg_catalog.pg_namespace nsp ON nsp.oid = connamespace
    join pg_catalog.pg_class fkt on fkt.oid = con.confrelid
	where nsp.nspname = p_schema_name
         and rel.relname = p_table_name;
$$;

comment on function _pg.get_table_foreign_keys is 'Retrieves all foreign keys for the specified table, including the table referenced';
