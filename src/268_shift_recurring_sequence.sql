create or replace function _pg.shift_recurring_sequence(p_num_elements int, p_current_element int, p_look_ahead_by int default 1, p_start_at int default 0)
returns table (se int)
immutable
parallel safe
language sql
as
$$
	with s as
	(
		select 	generate_series(p_start_at, p_num_elements - 1 + p_look_ahead_by + p_start_at) as q
	)
	select case when q > p_num_elements - 1 + p_start_at then q - p_num_elements else q end
		from s
		where q > p_current_element
		limit p_look_ahead_by;
$$;

comment on function _pg.shift_recurring_sequence is 'Generates the upcoming elements for a recurring sequence. Can be used, for example, to generate the list of the next 4 months in October - using the following call: select se + 1 as month_number
	from _pg.shift_recurring_sequence(p_num_elements := 12, p_current_element := 10, p_look_ahead_by := 4, p_start_at=>1)';
