CREATE OR REPLACE FUNCTION _pg.generate_random_string(p_length int)
 RETURNS text
 LANGUAGE sql
AS $function$
	with charset as
	(
		select c
		from
		(
			select	generate_series(65, 90) as c	-- 26
			union all
			select	generate_series(48, 57) as c	-- 10
			union all
			select	generate_series(97, 122) as c	--26
		) chars
		join
		(
			select generate_series(0, 10) as c1
		) multiplier on true
	),
	rand as
	(
		select string_agg(h.c, '') as generated_key
			from
			(
				select	row_number() over (order by random()) as rn,
						chr(c) as c
					from charset
					order by random()
			) h
	)
	select substring(generated_key, 0, p_length + 1) as api_token
		from rand r
$function$
;

comment on function _pg.generate_random_string is 'Generates a random strong of given length. No guarantees are given as to true randomness.';
