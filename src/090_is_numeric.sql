CREATE OR REPLACE FUNCTION _pg.is_numeric(p_str text)
 RETURNS boolean
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
  if (length(translate(p_str, '0123456789', '')) = 0) then
	    return true;
	elsif (length(translate(p_str, '0123456789', '')) = 1) and left(p_str, 1) = '-' then
		return true;
	else
		return false;
	end if;
end;
$function$
;

-- Tested this way vs catching an exception, the results are almost twice as good as the latter.

comment on function _pg.is_numeric is 'Checks if the supplied parameter is numeric.';