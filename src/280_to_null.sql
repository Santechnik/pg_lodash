create or replace function _pg.to_null(p_param text)
returns text
language plpgsql
immutable
parallel safe
as
$$
begin
	if (lower(p_param) = 'null' or lower(p_param) = 'nil') then
		return null;
	else
		return p_param;
	end if;
end;
$$;

comment on function _pg.to_null is 'Checks the input for a variation of a null string (e.g. NULL, null, nil) and return [NULL] if it is. Otherwise the original string is returned';
