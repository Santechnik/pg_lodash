create or replace function _pg.leading_zero(p_num integer, p_positions integer)
returns text
language sql 
as 
$$
	select lpad(p_num::text, p_positions, '0');
$$;

comment on function _pg.leading_zero is 'Adds the specified number of leading zeros.';