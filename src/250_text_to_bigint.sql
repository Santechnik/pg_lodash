
CREATE OR REPLACE FUNCTION _pg.text_to_bigint(text, bigint)
 RETURNS bigint
 LANGUAGE plpgsql
 IMMUTABLE
AS $function$
begin
    return cast($1 as bigint);
exception
    when invalid_text_representation then
        return $2;
end;
$function$
;

comment on function _pg.text_to_bigint is 'Converts text to bigint. If the conversion fails, returns the second argument';